# a test project for Ladbrokes
### ReactJS/Redux + Laravel + mysql 

## how to run the app
```bash
cd yourPath/ladapi
docker-compose up
```

## how to access the application
```bash
http://localhost:8080
```
## how to access the api server
```bash
# get meetings and events
curl -X GET http://localhost:8080/api/v1/next5?meeting=all
# get single event
curl -X GET http://localhost:8080/api/v1/next5?event=[event_id]
# get single meeting
curl -X GET http://localhost:8080/api/v1/next5?meeting=[meeting_id]
# get single meeting and single event
curl -X GET http://localhost:8080/api/v1/next5?meeting=[meeting_id]&event=[event_id]
```


## how to run testing of frondend
```bash
docker-compose exec spa bash
npm test
exit
```
## how to run testing of backend
```bash
docker-compose exec app bash
phpunit
exit
```

## the generated testing data is for 1 hour, how to generate new data
```bash
docker-compose exec app bash
php artisan db:seed
exit
# or directly run
docker-compose exec app php artisan db:seed
```

## how to clean
```bash
cd yourPath/ladapi
docker-compose down
```
